<!--<link href="<?php //echo base_url(); ?>public/bootstrap/css/login-style.css" rel='stylesheet' type='text/css' />-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfonts-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text.css'/>
<!--//webfonts-->

<div class="main_bg">
<div class="wrap">
	<?php if( ! isset( $optional_login ) ) : ?>
		<!--<h1>Login</h1>-->
	<?php endif; ?>

	<?php if( ! isset( $on_hold_message ) ) : ?>

		<?php if( isset( $login_error_mesg ) ) :?>
			<div style="border:1px solid red;">
				<p>
					Login Error: Invalid Username, Email Address, or Password.
				</p>
				<p>
					Username, email address and password are all case sensitive.
				</p>
			</div>
		<?php endif; ?>

		<?php if( $this->input->get('logout') ) : ?>
				<div style="border:1px solid green">
					<p>
						You have successfully logged out.
					</p>
				</div>
		<?php endif; ?>

	<div class="header" >
	<h1>Inicia sesisón o crea una cuenta</h1>
	</div>
	<p>Algún texto aquí.</p>
		<form>
			<ul class="left-form">
				<h2>Crear cuenta:</h2>
				<li>
					<input type="text"   placeholder="Usuario" required id="nombre"/>
					<div class="clear"> </div>
				</li> 
				<li>
					<input type="text"   placeholder="E-mail" required id="email"/>
					<div class="clear"> </div>
				</li>
				<li>
					<input type="text"   placeholder="Repetir E-mail" required id="emailConfir"/>
					<div class="clear"> </div>
				</li> 
				<li>
					<input type="password"   placeholder="Contraseña" required id="pwd"/>
					<div class="clear"> </div>
				</li> 
				<li>
					<input type="password"   placeholder="Repetir contraseña" required id="pwdConfir"/>
					<div class="clear"> </div>
				</li> 
				<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i> </i>Por favor infórmame con promociones y ofertas de la tienda.</label>
				<input type="submit" onclick="registrar()" value="Crear Cuenta">
				<div class="clear"> </div>
			</ul>
			</form>
			<ul class="right-form">
				<h3>Identifícate:</h3>
				<?php echo form_open( $login_url, array( 'class' => 'std-form' ) ); ?>
				<div>
					<label for="login_string" class="form_label">Username or Email</label>
					<input type="text" name="login_string" id="login_string" class="form_input" autocomplete="off" maxlength="255" />

					<br />

					<label for="login_pass" class="form_label">Password</label>
					<input type="password" name="login_pass" id="login_pass" class="form_input password" maxlength="<?php echo config_item('max_chars_for_password'); ?>" autocomplete="off" readonly="readonly" onfocus="this.removeAttribute('readonly');" />
					<?php if(config_item('allow_remember_me')) : ?>
						<br />
						<label for="remember_me" class="form_label">Remember Me</label>
						<input type="checkbox" id="remember_me" name="remember_me" value="yes" />
					<?php endif;?>

					<p>
						<a href="<?php echo secure_site_url('recover'); ?>">
							¿No puedes acceder a tu cuenta?
						</a>
					</p>
					<input type="submit" name="submit" value="Login" id="submit_button"  />
				</div>
				<div class="clear"> </div>
			</ul>
			<div class="clear"> </div>
	<?php else : ?>
		<div style="border:1px solid red;">
			<p>
				Excessive Login Attempts
			</p>
			<p>
				You have exceeded the maximum number of failed login<br />
				attempts that this website will allow.
			<p>
			<p>
				Your access to login and account recovery has been blocked for ' . ( (int) config_item('seconds_on_hold') / 60 ) . ' minutes.
			</p>
			<p>
				Please use the ' . secure_anchor('examples/recover','Account Recovery') . ' after ' . ( (int) config_item('seconds_on_hold') / 60 ) . ' minutes has passed,<br />
				or contact us if you require assistance gaining access to your account.
			</p>
		</div>
	<?php endif; ?>
</div>
</div>